# Server Provisioning and Configuration Playbook

* Introduction
* Playbook Tasks
* Ansible Role Variables
* Manual Execution of the Playbook

## Introduction

This Playbook was designed for the creation and configuration of AWS instances/VMs. The Playbook provides the ability to spin up a instance on a specified network with the ability to configure several standard configuration options for each system outlined in the *Playbook Options* section.


##  Playbook Tasks

## Ansible Role Variables

## Manual Execution of the Playbook

If you would like to run this Playbook from an Ansible Workstation you can follow these steps.

1. You'll need to clone or copy the repo to a location on the system.

```gitcli
git clone repo_name
```

2. In order to get it to run you'll need to modify the variables related to AWS
    * You'll need to edit */roles/common/vars/main.yml* 
3. You'll need a file that contains the vault password, in the example below a file *vault_pass.txt* contains the password for the vaults and is stored in your home folder.

```ansibleCLI
# Decrypt
ansible-vault decrypt --vault-password-file <pathToFile>

# Edit
# You can modify it with any text editor or use something like this
echo "PasswordHere" >> <pathToFile>

# Encrypt
ansible-vault encrypt --vault-password-file <pathToFile>

# View
ansible-vault view --vault-password-file <pathToFile>

```

4. After you have modified the files you can start the job with the following command at root of the repo.

```ansibleCLI
ansible-playbook -i hosts deploy.yml --vault-password-file ~/.vault_pass.txt
```
